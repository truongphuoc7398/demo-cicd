package com.example.demo.service;

import com.example.demo.entity.User;

public interface UserService {
    User getUser(Long id);

    User addUser(User user);
}
