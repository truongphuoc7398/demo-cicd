FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
COPY ./pom.xml .
RUN mvn clean verify -B
COPY ./src .
RUN mvn -B package -Dspring.profiles.active=dev

FROM openjdk:3.6.3-jdk-11-slim
RUN mkdir "app"
WORKDIR /app
COPY --from=MAVEN_BUILD root/target/demo-0.0.1-SNAPSHOT.jar /app/demo-cicd.jar
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=dev", "demo-cicd.jar"]